//
//  ViewController.swift
//  OneDrive
//
//  Created by Students on 14/05/2018.
//  Copyright © 2018 Students. All rights reserved.
//

import UIKit
import WebKit
class ViewController: UIViewController , WKUIDelegate, WKNavigationDelegate{
    
    var idkods: String?
    var token: String?

    @IBOutlet weak var datview: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let site =  "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=8ba75175-7463-4fa7-9e40-7515e718f5d3&scope=files.read&response_type=code&redirect_uri=msal8ba75175-7463-4fa7-9e40-7515e718f5d3://auth"
        

        let url = URL(string:site.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
      
        let open = URLRequest(url: url!)
        datview.navigationDelegate = self as WKNavigationDelegate;
        datview.load(open)
        
    
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
}
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
}
    
    
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        if (idkods != nil) {
       
        let url = URL(string: "https://login.microsoftonline.com/common/oauth2/v2.0/token")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let clientID = "8ba75175-7463-4fa7-9e40-7515e718f5d3"
            
            
        let postString = "client_id="+clientID+"&redirect_uri=msal8ba75175-7463-4fa7-9e40-7515e718f5d3://auth&code=\(idkods!)&grant_type=authorization_code"
            request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            self.webView2(responseString: responseString!)
            let dict = self.convertToDictionary(text: responseString!)
            let token =  dict!["access_token"] as! String
            self.getDrive(token: token)
        }
        task.resume()
        }
}
    
    func getDrive(token: String){
        
        let url = URL(string: "https://graph.microsoft.com/v1.0/me/drives/51420aa3cd61f800/root/children")!
        var request = URLRequest(url: url)
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print(responseString)
       
            DispatchQueue.main.async { [unowned self] in
                    self.datview.loadHTMLString(responseString!, baseURL: nil)
            }
            
        }
               task.resume()
}
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {

        let texts = navigationAction.request.url?.absoluteString
        if texts?.range(of:"code=") != nil {
            let textsArr = texts?.components(separatedBy: "=")

            idkods = textsArr![1]
            print(idkods!)
        }

        decisionHandler(WKNavigationActionPolicy.allow)
}
    
    
    func webView2(responseString: String) {
        
        
        let textsArr = responseString.components(separatedBy: ",")
        print(textsArr.last!)
        
}
    
    
}

